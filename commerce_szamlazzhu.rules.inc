<?php

/**
 * Implements hook_rules_action_info().
 */
function commerce_szamlazzhu_rules_action_info() {
  $actions = array();

  $actions['commerce_szamlazzhu_request_invoice'] = array(
    'label' => t('Request invoice'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
      ),
    ),
    'group' => 'Szamlazz.hu',
    'callbacks' => array(
      'execute' => 'commerce_szamlazzhu_request_invoice',
    ),
  );

  return $actions;
}

function commerce_szamlazzhu_request_invoice($order) {

  if (variable_get('commerce_szamlazzhu_username') != NULL && variable_get('commerce_szamlazzhu_password') != NULL) {

    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $customer = user_load($order_wrapper->uid->value());

    // Get customer name.
    $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();

    /* Setting customer name in the following order (fallback)
     * 0. Billing address company
     * 1. Billing address first and last name
     * 2. Billing address name line
     * 3. Owner's realname (realname module)
     * 4. Owner's username
     */

    if (!empty($billing_address['organisation_name'])) {
      $customer_name = $billing_address['organisation_name'];
    }
    else {
      $customer_name = $customer->name;
      if (!empty($customer->realname)) {
        $customer_name = $customer->realname;
      }
      if (!empty($billing_address['name_line'])) {
        $customer_name = $billing_address['name_line'];
      }
      if (!empty($billing_address['first_name']) && !empty($billing_address['last_name'])) {
        $customer_name = $billing_address['first_name'] . ' ' . $billing_address['last_name'];
      }
    }

    $xml = new DOMDocument('1.0', 'UTF-8');
    $xml_invoice = $xml->createElement('xmlszamla');
    $xml_invoice->setAttribute('xmlns', 'http://www.szamlazz.hu/xmlszamla');
    $xml_invoice->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    $xml_invoice->setAttribute('xsi:schemaLocation', 'http://www.szamlazz.hu/xmlszamla xmlszamla.xsd ');

    $xml_invoice_settings = $xml->createElement('beallitasok');
    $xml_invoice_settings->appendChild($xml->createElement('felhasznalo', variable_get('commerce_szamlazzhu_username')));
    $xml_invoice_settings->appendChild($xml->createElement('jelszo', variable_get('commerce_szamlazzhu_password')));
    $xml_invoice_settings->appendChild($xml->createElement('eszamla', variable_get('commerce_szamlazzhu_eszamla', TRUE)));
    $xml_invoice_settings->appendChild($xml->createElement('szamlaLetoltes', 'true'));
    $xml_invoice_settings->appendChild($xml->createElement('szamlaLetoltesPld', '2'));
    $xml_invoice_settings->appendChild($xml->createElement('valaszVerzio', '1'));
    $xml_invoice->appendChild($xml_invoice_settings);

    $xml_invoice_header = $xml->createElement('fejlec');
    $xml_invoice_header->appendChild($xml->createElement('keltDatum', date('Y-m-d')));
    $xml_invoice_header->appendChild($xml->createElement('teljesitesDatum', date('Y-m-d')));
    $xml_invoice_header->appendChild($xml->createElement('fizetesiHataridoDatum', date('Y-m-d')));
    $xml_invoice_header->appendChild($xml->createElement('fizmod', 'Készpénz'));
    $xml_invoice_header->appendChild($xml->createElement('penznem', 'Ft'));
    $xml_invoice_header->appendChild($xml->createElement('szamlaNyelve', 'hu'));
    $xml_invoice_header->appendChild($xml->createElement('megjegyzes', ''));
    $xml_invoice_header->appendChild($xml->createElement('elolegszamla', 'false'));
    $xml_invoice_header->appendChild($xml->createElement('vegszamla', 'false'));
    $xml_invoice_header->appendChild($xml->createElement('dijbekero', 'false'));
    $xml_invoice->appendChild($xml_invoice_header);

    $xml_invoice_seller = $xml->createElement('elado');
    $xml_invoice->appendChild($xml_invoice_seller);

    $xml_invoice_buyer = $xml->createElement('vevo');
    $xml_invoice_buyer->appendChild($xml->createElement('nev', $customer_name));
    $xml_invoice_buyer->appendChild($xml->createElement('irsz', $order_wrapper->commerce_customer_billing->commerce_customer_address->postal_code->value()));
    $xml_invoice_buyer->appendChild($xml->createElement('telepules', $order_wrapper->commerce_customer_billing->commerce_customer_address->locality->value()));
    $xml_invoice_buyer->appendChild($xml->createElement('cim', $order_wrapper->commerce_customer_billing->commerce_customer_address->thoroughfare->value()));
    $xml_invoice_buyer->appendChild($xml->createElement('email', $order_wrapper->mail->value()));
    $xml_invoice->appendChild($xml_invoice_buyer);

    $xml_invoice_line_items = $xml->createElement('tetelek');


    foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      $unit_price = $line_item_wrapper->commerce_unit_price->value();

      //If there is no 'include_tax' field, search for tax in the price components.
      //@TODO: this only works when there is only one tax included in the price. Change to a universal solution.
      if (empty($unit_price['data']['include_tax'])) {
        foreach ($unit_price['data']['components'] as $component_key => $component) {
          if (substr($component['name'], 0, 4) == 'tax|' && $component['included'] == 1) {
            $unit_price['data']['include_tax'] = substr($component['name'], 4);
          }
        }
      }

      $total_price = $line_item_wrapper->commerce_total->value();
      $vat = (!empty($unit_price['data']['include_tax'])) ? commerce_price_component_load($unit_price, 'tax|' . $unit_price['data']['include_tax']) : NULL;
      $vat_rate = ($vat !== NULL) ? $vat[0]['price']['data']['tax_rate']['rate'] : 0;

      $net_unit_price = $unit_price['amount'] / (1 + $vat_rate);
      $net_amount = round($net_unit_price * $line_item_wrapper->quantity->value());
      $vat_amount = round($net_unit_price * $line_item_wrapper->quantity->value() * $vat_rate);

      $product_uom = t('pcs');
      if ($line_item_wrapper->type->value() == 'product') {
        $product = entity_metadata_wrapper('commerce_product', $line_item_wrapper->commerce_product->value());
        $product_title = $product->title->value() . ' (' . $line_item_wrapper->line_item_label->value() . ')';
        if (!empty($product->field_uom)) {
          $product_uom = $product->field_uom->value();
        }
        if (!empty($product->field_product_presentation)) {
          $product_uom = $product->field_product_presentation->value()->name;
        }
      }
      else {
        $product_title = $line_item_wrapper->line_item_label->value();
      }

      $xml_invoice_line_item = $xml->createElement('tetel');
      $xml_invoice_line_item->appendChild($xml->createElement('megnevezes', $product_title));
      $xml_invoice_line_item->appendChild($xml->createElement('mennyiseg', $line_item_wrapper->quantity->value()));
      $xml_invoice_line_item->appendChild($xml->createElement('mennyisegiEgyseg', $product_uom));
      $xml_invoice_line_item->appendChild($xml->createElement('nettoEgysegar', $net_unit_price));
      $xml_invoice_line_item->appendChild($xml->createElement('afakulcs', $vat_rate * 100));
      $xml_invoice_line_item->appendChild($xml->createElement('nettoErtek', $net_amount));
      $xml_invoice_line_item->appendChild($xml->createElement('afaErtek', $vat_amount));
      $xml_invoice_line_item->appendChild($xml->createElement('bruttoErtek', $total_price['amount']));
      $xml_invoice_line_items->appendChild($xml_invoice_line_item);
    }

    $xml_invoice->appendChild($xml_invoice_line_items);

    $xml->appendChild($xml_invoice);

    $xmltext = $xml->saveXML();

    $drupal_tmpfname = drupal_tempnam('private://', "szamlazzxml");
    $tmpfname = drupal_realpath($drupal_tmpfname);

    $handle = fopen($tmpfname, "w");
    fwrite($handle, $xmltext);
    fclose($handle);

    $agent_url = 'https://www.szamlazz.hu/szamla/';
    $download_invoice = TRUE;
    $ch = curl_init($agent_url);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    //From PHP 5.5, use CURLFile class to handle file upload.
    if (class_exists('CURLFile')) {
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('action-xmlagentxmlfile' => new CURLFile($tmpfname, 'application/xml')));
      curl_setopt($ch, CURLOPT_SAFE_UPLOAD, TRUE);
    } else {
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('action-xmlagentxmlfile' => '@' . $tmpfname));
    }

    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

    if ($_SESSION['szamlazz_cookie']) {
      curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['szamlazz_cookie']);
    }
    $agent_response = curl_exec($ch);

    $http_error = curl_error($ch);

    $agent_http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

    $agent_header = substr($agent_response, 0, $header_size);

    preg_match_all('|Set-Cookie: (.*);|U', $agent_header, $cookie_results);
    $_SESSION['szamlazz_cookie'] = implode(';', $cookie_results[1]);

    $agent_body = substr($agent_response, $header_size);

    curl_close($ch);

    //Delete temp xml file.
    drupal_unlink($drupal_tmpfname);

    $header_array = explode("\n", $agent_header);

    $is_error = FALSE;

    $agent_error = '';
    $agent_error_code = '';
    $invoice_number = '';

    foreach ($header_array as $val) {
      if (substr($val, 0, strlen('szlahu')) === 'szlahu') {
        if (substr($val, 0, strlen('szlahu_error:')) === 'szlahu_error:') {
          $is_error = TRUE;
          $agent_error = substr($val, strlen('szlahu_error:'));
        }
        if (substr($val, 0, strlen('szlahu_error_code:')) === 'szlahu_error_code:') {
          $is_error = TRUE;
          $agent_error_code = substr($val, strlen('szlahu_error_code:'));
        }
        if (substr($val, 0, strlen('szlahu_szamlaszam:')) === 'szlahu_szamlaszam:') {
          $invoice_number = trim(substr($val, strlen('szlahu_szamlaszam:')));
        }
      }
    }

    if ($http_error != "") {
      watchdog('commerce_szamlazzhu', 'Error while generating invoice: @message', array('@message' => $http_error), WATCHDOG_ERROR);
      throw new Exception(t('Error while generating invoice.') . ' ' . $http_error);
    }

    if ($is_error) {

      watchdog('commerce_szamlazzhu', 'Error while generating invoice: code: @code, hibaüzenet: @message', array(
        '@code' => $agent_error_code,
        '@message' => urldecode($agent_error)
      ), WATCHDOG_ERROR);

      throw new Exception(t('Unable to create invoice.') . $agent_error_code);
    }
    else {

      if ($download_invoice) {
        $invoice_dir = 'private://invoices';
        file_prepare_directory($invoice_dir, FILE_CREATE_DIRECTORY);
        $file = file_save_data($agent_body, $invoice_dir . '/' . $order_wrapper->order_id->value() . '-' . $invoice_number . '.pdf', FILE_EXISTS_RENAME);
        $file->display = 1;
        $file->description = "";
        $file = file_save($file);
        $files = $order_wrapper->field_invoice_attached->value();
        if (!$files) {
          $files = array();
        }
        $files[] = (array) $file;
        $order_wrapper->field_invoice_attached->set($files);
        $order_wrapper->save();
      }
    }

  }
  else {
    watchdog('commerce_szamlazzhu', 'Szamlazz.hu username and password not set. No invoice has bee reqested. See commerce_szamlazzhu/README.txt', WATCHDOG_WARNING);
  }
}
