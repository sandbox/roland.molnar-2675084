This module provides a simple rule action to request invoice from szamlazz.hu

Installation
1. Enable the module
2. Set the following variables either by using drush vset or adding them to the settings.php:

- commerce_szamlazzhu_username
- commerce_szamlazzhu_password
The szamlazz.hu username and password (same as using the web client)

- commerce_szamlazzhu_eszamla
true (default if not set): request electronic invoice (in PDF)
false: request normal printable invoice (also in PDF)
